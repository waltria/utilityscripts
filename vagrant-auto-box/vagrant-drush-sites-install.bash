#!/usr/local/bin/bash -x

# This script is the second of a 2 script process.
# The first script is run whilst the user is on the
# host and should reside in a dir on the host at the 
# same directory level as this vm, e.g.:
# HOST:
# - parent dir
#   - vm dir
#   - scripts dir that contains setup.bash

# Constants
repoPredicate="git@bitbucket.org:wwuweb"
drushAliasesDirName="collegesites-drush-aliases"
drushAliasesRepoName="collegesites-drush-aliases"
drushScriptsDirName="drupal-scripts"
drushScriptsRepoName="drupal-scripts"

# Do everything in home dir
cd
mkdir .drush
dir=`pwd` 

# Copy the vm starter dotfiles into the homedir.
#tar xvf /vagrant/home-dir.tar .
cp /vagrant/dotfiles.zip .;unzip -o dotfiles.zip;rm dotfiles.zip;cd

# Copy host's public/private keys to this box.
cp /vagrant/ssh-files.zip ./.ssh;cd ./.ssh;unzip -o ssh-files.zip;rm ssh-files.zip;cd

# Get the drush aliases:
cd /vagrant;git clone "$repoPredicate"/"$drushAliasesRepoName".git;cd "$drushAliasesDirName";chmod +x build.sh;./build.sh;cd

# Get the drush scripts.
cd /vagrant;git clone "$repoPredicate"/"$drushScriptsRepoName".git;cd
exit 0
