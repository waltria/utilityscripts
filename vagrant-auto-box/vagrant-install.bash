#!/usr/local/bin/bash
# This script should reside within a dir at the same level as the desired vagrant 
# install location.  As in:
# - Somedir
#   - Where I want vagrant to be installed
#   - vagrant-utils
#     - this script
#
# Constants:
# Sometimes repos explode into a toplevel dir with diff name than repo name.
vagrantboxDirName="wwu-vagrant-nfs" 
vagrantboxRepoName="wwu-vagrant-nfs"
repoPredicate="git@bitbucket.org:wwuweb"
sisterScript="vagrant-drush-sites-install.bash" 
vagrantDir="../$vagrantboxDirName"

# Mark the current location
dir=`pwd` 

# Set the root to the parent of the current directory.
rootdir="$(dirname "$dir")"

# Remove vagrant box then checkout latest vagrant box:
cd "$vagrantDir";vagrant halt;cd ..;rm -rf "$vagrantboxDirName";git clone "$repoPredicate"/"$vagrantboxRepoName".git;cd "$dir"

# Run vagrant utilities:
cd "$vagrantDir";vagrant plugin install vagrant-librarian-puppet;vagrant plugin install vagrant-vbguest;vagrant plugin install vagrant-bindfs

cd "$dir"

# Bundle up your ssh files:
zip -qj ssh-files $HOME/.ssh/id_rsa $HOME/.ssh/id_rsa.pub

# Copy the dotfiles and ssh-files to the vagrant box
cp dotfiles.zip "$vagrantDir"
cp ssh-files.zip "$vagrantDir"
cp "$sisterScript"  "$vagrantDir"

# Send message to user to login to vagrant box and run sister script.
echo "You need to run: 'cd $vagrantDir' then 'vagrant up'.  After the vagrant box is up you need to 'vagrant ssh' and run the 'bash /vagrant/$sisterScript' script"
exit 0 
