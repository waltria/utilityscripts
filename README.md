# README #

A repository for utility scripts.  

### What is this repository for? ###

* Quick summary
  Things to accomplish the mundane so our lives are more humane.
* Version
  1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
  None.  Runs bash.
* Configuration
  None.
* Dependencies
  None.
* Database configuration
  None.
* How to run tests
  Append -x to the #!/bin/bash
* Deployment instructions
  - If you're wanting to use the vagrant-auto-box scripts then
    you need to copy the vagrant-auto-box folder into the directory
    under which you'd like your vagrant box created.
    - Also, make note of the directions that get printed after 
      running the install script.

### Contribution guidelines ###

* Writing tests
  Try to test them first before pushing.
* Code review
  Sure.
* Other guidelines
  Buyer beware.  Should probably read the scripts before 
  running since we do make assumptions.  They're short.

### Who do I talk to? ###

* Repo owner or admin
  Alex ?
* Other community or team contact
  WebTech team
